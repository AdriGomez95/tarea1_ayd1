
## Toma de Requerimientos :pencil2:
### Antecedentes del proyecto
En la actualidad, existen hoteles que ofrecen sus servicios en todo el mundo, pero a falta de una página que los administre, los turistas no tienen el conocimiento si el hotel ofrece un buen o mal servicio; de igual manera con los vuelos de una aerolínea en especifico o la renta de autos que ofrece una empresa. Con la finalidad de poder ofrecerle a los turistas una opción para que puedan calificar los servicios que adquieren, se pueden evitar futuros disgustos respecto a los malos servicios y así, poder brindarle al turista la mejor experiencia con sus servicios adquiridos.

### Necesidades
Se necesita una página web responsive que sea capaz de registrar usuarios y servicios.
Dicha página deberá de brindar al usuario una agradable experiencia, siendo lo más interactiva posible y con la menor cantidad de validaciones al momento de planificar adquirir los servicios. También debe de ser capaz de brindar reseñas para que los turistas puedan opinar sobre sus experiencias al adquirir los servicios y puedan elegir la mejor opción. 

### Requerimientos funcionales
Gestión de administrador
- Creación de servicios
- Visualización de reseñas de los servicios dadas por turistas

Gestión de turistas
- Registro de cuenta
- Autenticación de cuenta
- Modificación de datos personales
- Encuestas por servicio adquirido (reseñas)
- Visualización reseñas de los servicios
- Realizar alquiler de auto
- Realizar reservación de hotel
- Realizar reservación de vuelo

Gestión de hoteles
- Registro de hotel
- Creación de habitación 
- Filtros de búsqueda por precio y fecha
- Visualización reseñas del servicio dadas por turistas

Gestión de autos
- Registro de autos
- Filtros de búsqueda por marca, modelo y precio
- Visualización reseñas del servicio dadas por turistas

Gestión de vuelos
- Registro de vuelos
- Creación de asientos en el vuelo
- Filtros de búsqueda por precio y destino
- Visualización reseñas del servicio dadas por turistas

### Requerimientos no funcionales
- Sitio web responsive
- Manejo de cookies en el sitio web
- Dashboard con visualización de todos los servicios para el administrador
- Implementación de navbar en el sitio web para una navegación fácil e intuitiva
- El sistema debe de ser capaz de soportar más de 10K usuarios y mantenerse funcionando correctamente
- Alertas de ofertas por correo
- Seguridad de la página
- Seguridad de la base de datos
- Encriptación de contraseñas
- Tiempo de respuesta del sitio web menor a 15 segundos


