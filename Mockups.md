
## Mockups de las principales vistas para la pagina web :pencil2:
### Confirmacion de Cuenta
![Confirmacion de Cuenta](https://i.imgur.com/bQ1Aeiu.png)

### Landing Administrador
![Landing Administrador](https://i.imgur.com/OMdHbbT.png)

### Landing Turista
![Landing Turista](https://i.imgur.com/vBOLJz5.png)

### Login
![Login](https://i.imgur.com/0OAzlOp.png)

### Mi Cuenta
![Mi Cuenta](https://i.imgur.com/keZr9da.png)

### Mis Reseñas
![Mis Reseñas](https://i.imgur.com/2IIiTYM.png)

### Mis Servicios
![Mis Servicios](https://i.imgur.com/OuCZTWR.png)

### Registro de Habitaciones
![Registro de Habitaciones](https://i.imgur.com/VMVmqFQ.png)

### Registro de Proveedores de Servicio
![Registro de Proveedores de Servicio](https://i.imgur.com/2TnZEu3.png)

### Registro de Usuarios
![Registro de Usuarios](https://i.imgur.com/TQsfAz3.png)

### Registro de Vehiculos
![Registro de Vehiculos](https://i.imgur.com/3MiTndK.png)

### Registro de Vuelos
![Registro de Vuelos](https://i.imgur.com/z23lnoa.png)

### Reservar Habitacion
![Reservar Habitacion](https://i.imgur.com/4SplL3w.png)

### Reservar Vehiculo
![Reservar Vehiculo](https://i.imgur.com/WHRKPST.png)

### Reservar Vuelo
![Reservar Vuelo](https://i.imgur.com/zWXm3JQ.png)
