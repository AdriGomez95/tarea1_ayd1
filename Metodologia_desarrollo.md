
## Metodología de desarollo :pencil2:
La metodología escogida para el desarollo del  proyecto fue la metodología **Scrum** que en pocas palabras es un marco de trabajo de adaptación iterativa e incremental, eficaz
diseñado para obtener resultados dentro de un proyecto de manera rápida y efica.
Una ventaja de que posee Scrum es que es ligero, fácil de entender. Se sabe que a diferencia de las demas tecnologías **Scrum** aporta un ritmo de trabajo sostenible , siendo capaz de poder adaptarse a cualquier cambio de manera inmediata. 
Por otra parte una desventaja que posee **Scrum** a diferencia de las demás metolodogías de trabajo es que al momento de querer implementar  dicha metodología en nuestro proyecto presenta ciertas dificultades al querer implementarlo. Un ejemplo de ello es que quienes utilicen esta metodología  tienen que tener una alta formación  en el tema , ya que tambíen otra desventaja es que al momento de querer implementarlo requiere una exhaustiva definición de las tareas a realizar. 
El porque se uso la metodología **Srum** en el proyecto se contesta de una manera facíl y sencilla ya que es un proyecto de especificamente de software esta a sujeto a muchos cambios y ya que como  **Scrum** se adapta facilmente a los cambios es una metodología acorde al proyecto a implementar.
El equipo esta desarollo Scrum Master del proyecto será Adriana Marié Gómez Dávila, 
El product Owner del proyecto será el auxiliar del curso Manuel Alejandra de Mata. 
El development  team  estará  conformado por : 
- Pablo Daniel Rivas
- Juan Diego Alvarado Salguero
- José Ottoniel Sincal Guitzol
- Eleazar Jared Lopez Osuna

