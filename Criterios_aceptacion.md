
## Historias de usuario/criteros de aceptacion :pencil2:
### Sitio web resposive
```
Criterio de aceptación

1. Como DEV quiero que la interfaz grafica sea responsive.
2. Realizando respectivos cambios en los archivos css para dejar la pagina responsive.
3. Esto dara una interacion a la pagina agradable para el usuario
```

### Compatibilidad de navegadores
```
Criterio de aceptación

1. Como DEV quiero que el navegador tenga compatibilidad.
2. Realizando una configuracion en el frontend.
3. Esto admitira cualquier explorador web donde el usuario acceda al cliente.
```

### Registro de administradores
```
Criterio de aceptación

1. Como DEV quiero un usuario que administre toda la plataforma.
2. Creando un usuario con rol administrador.
3. Esto brindará un mejor control de servicios en la página.
```

### Autenticación de cuenta
```
Criterio de aceptación

1. Como DEV quiero tener cuentas reales registradas.
2. Realizando una autenticación en 2 pasos para verificar cuentas.
3. Esto hará la plataforma más segura para los usuarios.
```

### Observar reseñas de servicios
```
Criterio de aceptación

1. Como DEV quiero un ambiente donde se muestren las reseñas de los servicios.
2. Realizando una seccion en la parte grafica donde se visualizan las reseñas.
3. Esto hara que los usuarios visualicen las reseñas de los servicios en la interfaz grafica.
```

### Creación de hoteles
```
Criterio de aceptación

1. Como DEV quiero un formulario que registre los datos de un hotel para registrarlo.
2. Realizando un formulario que acepte varios campos de informacion y consultarlos al momento de hacer la insersion a base de datos.
3. Esto permitira que la parte administrativa pueda registrar hoteles.
```

### Creación de autos
```
Criterio de aceptación

1. Como DEV quiero un formulario que registre los datos de un auto para registrarlo.
2. Realizando un formulario que acepte varios campos de informacion y consultarlos al momento de hacer la insersion a base de datos.
3. Esto permitira que la parte administrativa pueda registrar autos.
```

### Creación de vuelos
```
Criterio de aceptación

1. Como DEV quiero un formulario que registre los datos de un vuelo para registrarlo.
2. Realizando un formulario que acepte varios campos de informacion y consultarlos al momento de hacer la insersion a base de datos.
3. Esto permitira que la parte administrativa pueda registrar vuelos.
```

### Registro de usuarios turistas
```
Criterio de aceptación

1. Como DEV quiero un formulario que registre los datos de un usuario turista para registrarlo.
2. Realizando un formulario que acepte varios campos de informacion y consultarlos al momento de hacer la insersion a base de datos.
3. Esto permitira que la parte administrativa pueda registrar usuarios turistas.
```

### Autenticación de cuentas
```
Criterio de aceptación

1. Como DEV quiero un sistema de autenticacion de las cuentas
2. Realizando un solicitud de codigo de acceso via correo.
3. Esto permitira autenticar que los que exista veracidad con los usuarios que estan ingresando.
```

### Modificaciones de datos personales
```
Criterio de aceptación

1. Como DEV quiero un formulario que muestre los datos de un usuario y permita editarlos.
2. Realizando un formulario que permita visualizar y aceptar cambios en los campos de informacion y consultarlos al momento de hacer la actualizacion a base de datos.
3.
```

### Foto de perfil por usuario
```
Criterio de aceptación

1. Como DEV quiero un componente que permita la insercion y actualizacion de una foto de perfil en la cuenta del usuario.
2. Realizando un componente que permita adjuntar archivos de imagenes para que puedan ser subidas y guardadas en la nube.
3. Esto permitira que que los usuarios puedan agregar fotos de perfil a su cuenta de usuario.
```

### Encuestas de servicio adquirido 
```
Criterio de aceptación

1. Como DEV quiero un componente que solicite un formulario donde se soliciten opiniones del usuario donde opinen sobre el servicio adquirido.
2. Realizando un componente que ofrezca un formulario y realizar una insercion en base de datos.
3. Esto permitira que los usuarios registren opiniones para dar retroalimentacion de los servicios adquiridos.
```


### Realizar reservación de habitación
```
Criterio de aceptación

1. Como DEV quiero un componente que permita el registro de reservacion de habitaciones que soliciten los usuarios.
2. Realizando un componete que solicite un formulario que registre los datos de la reservacion e inserte en base de datos.
3. Esto permitira a los usuarios realizar reservaciones de habitaciones desde la pagina web.
```

### Realizar alquiler de auto
```
Criterio de aceptación

1. Como DEV quiero un componente que permita el registro de los alquileres de autos.
2. Realizando un componete que soliciten un formulario que registre los datos del alquiler e inserte en base de datos.
3. Esto permitira a los usuarios realizar alquiler de autos desde la pagina web.
```

### Realizar compra de vuelo
```
Criterio de aceptación

1. Como DEV quiero un componente que permita la compra de un vuelo.
2. Realizando un componete que soliciten un formulario que registre los datos del alquiler e inserte en base de datos.
3. Esto permitira a los usuarios realizar compras de vuelos desde la pagina web.
```

### Registro de hoteles
```
Criterio de aceptación

1. Como DEV quiero un componente que permita el registro de los hoteles.
2. Realizando un comoponete que soliciten un formulario que registre los hoteles e inserte en base de datos.
3. Esto permitira a los usuarios realizar registros a hoteles desde la pagina web.
```

### Creación de habitaciones
```
Criterio de aceptación

1. Como DEV quiero un formulario que registre los datos de una habitacion para crear el registro.
2. Realizando un formulario que acepte varios campos de informacion y consultarlos al momento de hacer la insersion a base de datos.
3. Esto permitira que la parte administrativa pueda registrar habitacioens.
```

### Creación de disponibilidad de habitación
```
Criterio de aceptación

1. Como DEV quiero un componete que muestre los datos de una habitacion disponible.
2. Realizando un componente que muestre la informacion de las habitaciones y verifique que tenga disponibilidad.
3. Esto permitira que la parte administrativa pueda crear tarjetas de habitaciones disponibles.
```

### Observar reseñas de hotel y habitaciones
```
Criterio de aceptación

1. Como DEV quiero un registro de reseñas de las habitaciones dadas por los turistas.
2. Agregando un dashboard a los hoteles donde se puedan visualizar todas las reseñas de sus habitaciones.
3. Esto permitirá visualizar la calidad de estadía que brindaron los hoteles en sus habitaciones.
```

### Cantidad de habitaciones a reservar
```
Criterio de aceptación

1. Como DEV quiero que en el componente de las habitaciones reservadas se solicite una cantidad de habitaciones a reservar.
2. Agregando una seccion con un contador que mustre diferentes cantidades numericas.
3. Esto permitira al usuario agregar una cantidad de habitaciones al momento de solicitar la reservacion.
```

### Verificación de disponibilidad de habitación
```
Criterio de aceptación

1. Como DEV quiero que los turistas puedan rentar habitaciones libres en un hotel.
2. Realizando un endpoint en el backend que verifique la disponibilidad de la habitacion del hotel en la base de datos.
3. Esto permitirá que los turistas puedan verificar las habitaciones disponibles en los hoteles.
```

### Filtros de búsqueda por fecha
```
Criterio de aceptación

1. Como DEV quiero que los servicios puedan filtrarse por fecha.
2. Realizando una tabla que liste todos los servicios y agregando un textbox para escribir la opción de filtro.
3. Esto permitirá que los turistas puedan buscar servicios según sus fechas preferidas.
```

### Registro de autos
```
Criterio de aceptación

1. Como DEV quiero un formulario que registre los datos de un auto para crear el registro.
2. Realizando un formulario que acepte varios campos de informacion y consultarlos al momento de hacer la insersion a base de datos.
3. Esto permitira que la parte administrativa pueda registrar los autos.
```

### Creación de disponibilidad de auto
```
Criterio de aceptación

1. Como DEV quiero un componete que muestre los datos de un auto disponible.
2. Realizando un componente que muestre la informacion de los autos y verifique que tenga disponibilidad.
3. Esto permitira que la parte administrativa pueda crear tarjetas de autos disponibles.
```

### Observar reseñas de autos
```
Criterio de aceptación

1. Como DEV quiero un registro de reseñas de autos dadas por los turistas.
2. Agregando un dashboard a los autos donde se puedan visualizar todas las reseñas.
3. Esto permitirá visualizar la calidad de la renta de auto que brindaron las empresas.
```

### Cantidad de autos a rentar
```
Criterio de aceptación

1. Como DEV quiero que en el componente de los rentar autos se solicite una cantidad de autos a rentar.
2. Agregando una seccion con un contador que mustre diferentes cantidades numericas.
3. Esto permitira al usuario agregar una cantidad de autos al momento de solicitar la renta.
```

### Verificación de disponibilidad de auto
```
Criterio de aceptación

1. Como DEV quiero que los turistas puedan rentar autos disponibles.
2. Realizando un endpoint en el backend que verifique la disponibilidad del auto en la base de datos.
3. Esto permitirá que los turistas puedan verificar los autos disponibles a rentar.
```

### Filtros de búsqueda por marca y modelo
```
Criterio de aceptación

1. Como DEV quiero que los autos puedan filtrarse por marca y modelo.
2. Realizando una tabla que liste todos los autos y agregando un textbox para escribir la opción de filtro.
3. Esto permitirá que los turistas puedan buscar los autos según sus preferencias de marca y modelo. 
```

### Registro de fecha de vuelo
```
Criterio de aceptación

1. Como DEV quiero un formulario que registre los datos de un vuelo para crear el registro.
2. Realizando un formulario que acepte varios campos de informacion y consultarlos al momento de hacer la insersion a base de datos.
3. Esto permitira que la parte administrativa pueda registrar los vuelos.
```


### Observar reseñas de vuelos
```
Criterio de aceptación

1. Como DEV quiero un registro de reseñas de vuelos dadas por los turistas.
2. Agregando un dashboard a los vuelos donde se puedan visualizar todas las reseñas.
3. Esto permitirá visualizar la calidad de vuelo que brindaron las aerolineas.
```

### Fecha del viaje a realizar
```
Criterio de aceptación

1. Como DEV quiero que en el componente de viajes  se solicite un horario de reservacion.
2. Agregando una seccion con un contador que mustre diferentes horarios de viaje.
3. Esto permitira al usuario agregar una fecha de viaje al momento de solicitar un viaje.
```

### Cantidad de asientos a reservar
```
Criterio de aceptación

1. Como DEV quiero que los turistas puedan comprar asientos libres en un vuelo.
2. Realizando un endpoint en el backend que verifique la disponibilidad del asiento en la base de datos.
3. Esto permitirá que los turistas puedan tener una mejor experiencia en sus vuelos con sus asientos reservados.
```

### Verificación de disponibilidad de asientos
```
Criterio de aceptación

1. Como DEV quiero que los turistas puedan comprar asientos libres en un vuelo.
2. Realizando un endpoint en el backend que verifique la disponibilidad del asiento en la base de datos.
3. Esto permitirá que los turistas puedan verificar los asientos disponibles en los vuelos.
```

### Filtros de búsqueda por destino
```
Criterio de aceptación

1. Como DEV quiero que los servicios puedan filtrarse por destino.
2. Realizando una tabla que liste todos los servicios y agregando un textbox para escribir la opción de filtro.
3. Esto permitirá que los turistas puedan buscar servicios según sus preferencias.
```

### Filtros de búsqueda por precio
```
Criterio de aceptación

1. Como DEV quiero que los servicios puedan filtrarse por precios.
2. Realizando una tabla que liste todos los servicios y agregando un textbox para escribir la opción de filtro.
3. Esto permitirá que los turistas puedan buscar servicios según sus preferencias. 
```
