
## Descripcion de las tecnologias a utilizar :pencil2:
### React :atom_symbol:
 React es una librería Javascript focalizada en el desarrollo de interfaces de usuario. Así se define la propia librería y evidentemente, esa es su principal área de trabajo. Sin embargo, lo cierto es que en React encontramos un excelente aliado para hacer todo tipo de aplicaciones web, SPA (Single Page Application) o incluso aplicaciones para móviles. Para ello, alrededor de React existe un completo ecosistema de módulos, herramientas y componentes capaces de ayudar al desarrollador a cubrir objetivos avanzados con relativamente poco esfuerzo.

Por tanto, React representa una base sólida sobre la cual se puede construir casi cualquier cosa con Javascript. Además facilita mucho el desarrollo, ya que nos ofrece muchas cosas ya listas, en las que no necesitamos invertir tiempo de trabajo. En este artículo te ampliaremos esta información, aportando además diversos motivos por los que usar React como librería del lado del cliente. 

#### Ventajas
* Facilita el proceso general de escritura de componentes.
* Aumenta la productividad y facilita un mayor mantenimiento
* Asegura un renderizado más rápido
* Garantiza código estable
* Es compatible con SEO
* Viene con un útil conjunto de herramientas para desarrolladores
* Existe React Native para el desarrollo de aplicaciones móviles
* Está enfocado y es fácil de aprender
* Está respaldado por una comunidad sólida

### Python - Flask :snake:
Flask es un “micro” Framework: Para desarrollar una App básica o que se quiera desarrollar de una forma ágil y rápida Flask puede ser muy conveniente, para determinadas aplicaciones no se necesitan muchas extensiones y es suficiente.
Incluye un servidor web de desarrollo: No se necesita una infraestructura con un servidor web para probar las aplicaciones sino de una manera sencilla se puede correr un servidor web para ir viendo los resultados que se van obteniendo.
Tiene un depurador y soporte integrado para pruebas unitarias: Si tenemos algún error en el código que se está construyendo se puede depurar ese error y se puede ver los valores de las variables. Además está la posibilidad de integrar pruebas unitarias.

#### Ventajas
* Se adapta a cada proyecto instalando extensiones específicas para el mismo.
* Incluye servidor web propio para pruebas.
* El diseño minimalista de su estructura le permite ser rápido y con un gran desempeño.
* Cuenta con documentación extensa para el desarrollo de aplicaciones.

### MySQL :dolphin:
El sistema MySQL tiene varias peculiaridades. En primer lugar, es un software de código abierto, lo que significa que puede ser usado libremente y modificarlo a voluntad. Por ese motivo, cualquier persona puede instalarlo y personalizar el código fuente para satisfacer sus necesidades específicas.

#### Ventajas
* Es una base de datos gratuita. Al ser de código abierto, no tiene coste, con el ahorro que eso conlleva.
* Es muy fácil de usar. Podemos empezar a usar la base de datos MySQL sabiendo unos pocos comandos.
* Es una base de datos muy rápida. Su rendimiento es estupendo sin añadirle ninguna funcionalidad avanzada.
* Utiliza varias capas de seguridad. Contraseñas encriptadas, derechos de acceso y privilegios para los usuarios.
* Pocos requerimientos y eficiencia de memoria. Tiene una baja fuga de memoria y necesita pocos recursos de CPU o RAM.
* Es compatible con Linux y Windows.

### AWS :package:
AWS está diseñado para permitir que los proveedores de aplicaciones, los proveedores de software independientes y los distribuidores puedan hospedar de una forma rápida y segura su aplicación, tanto si es una aplicación existente como si es una nueva aplicación basada en SaaS. Puede utilizar la consola de administración de AWS o las API de servicio web bien documentadas para obtener acceso a la plataforma de hospedaje de aplicaciones de AWS.
AWS le permite seleccionar el sistema operativo, el lenguaje de programación, la plataforma de aplicaciones web, la base de datos, así como el resto de servicios que necesite. Con AWS, tendrá acceso a un entorno virtual que le permite cargar el software y los servicios que necesita su aplicación. Esto facilita el proceso de migración de las aplicaciones existentes y mantiene las opciones para crear nuevas soluciones.
AWS tambien aplica un enfoque integral para proteger y reforzar nuestra infraestructura, incluidas medidas físicas, operativas y de software. Para obtener más información, consulte el Centro de seguridad de AWS.

### Docker :whale2:
Docker es una plataforma de software que le permite crear, probar e implementar aplicaciones rápidamente. Docker empaqueta software en unidades estandarizadas llamadas contenedores que incluyen todo lo necesario para que el software se ejecute, incluidas bibliotecas, herramientas de sistema, código y tiempo de ejecución. Con Docker, puede implementar y ajustar la escala de aplicaciones rápidamente en cualquier entorno con la certeza de saber que su código se ejecutará.
La ejecución de Docker en AWS les ofrece a desarrolladores y administradores una manera muy confiable y económica de crear, enviar y ejecutar aplicaciones distribuidas en cualquier escala.

#### Ventajas
* Simplicidad y configuraciones rápidas
* Estandarización
* Despliegue rápido
* Inversión y costos
* Aislamiento de recursos

## Lucidchart :triangular_ruler:
Lucidchart es una herramienta de diagramación basada en la web, que permite a los usuarios colaborar y trabajar juntos en tiempo real, creando diagramas de flujo, organigramas, esquemas de sitios web, diseños UML, mapas mentales, prototipos de software y muchos otros tipos de diagrama.

### Ventajas
* Facil uso
* Trabajo grupal de manera sencilla
* Multiples formatos de export

## MySQL Workbench
MySQL Workbench es una herramienta visual de diseño de bases de datos que integra desarrollo de software, administración de bases de datos, diseño de bases de datos, gestión y mantenimiento para el sistema de base de datos MySQL.

### Ventajas
* Interfaz amigable
* Baja curva de aprendizaje
* Generacion de codigo DDL

## Google meet
Google Meet es la aplicación de videoconferencias de Google, para navegadores web y dispositivos móviles, enfocada al entorno laboral y que sustituye a Google Hangouts, dentro de G-Suite, el pack de aplicaciones de Google para profesionales.

### Ventajas
* Posibilidad para acceder desde cualquier dispositivo
* Facilidad para compartir contenido

## Draw.io
Draw.io es una herramienta de diagramación basada en la web, que permite a los usuarios colaborar y trabajar juntos en tiempo real, creando diagramas de flujo, organigramas, esquemas de sitios web, diseños UML, mapas mentales, prototipos de software y muchos otros tipos de diagrama.

### Ventajas
* Facil uso
* Trabajo grupal de manera sencilla
* Multiples formatos de export
* Gratuita

## Trello
Trello es una herramienta visual que permite a los equipos gestionar cualquier tipo de proyecto y flujo de trabajo, así como supervisar tareas. Añade archivos, checklists o incluso automatizaciones: personalízalo todo según las necesidades de tu equipo.

### Ventajas
* Facil trabajo en grupo
* Manejo de tarjetas
* Multiples listas de tarjetas
* Etiquetado de tarjetas por categoria
* Asignacion y notificacion de tarjetas

## Miro
Miro es una pizarra digital colaborativa en línea, que puede ser usada para la investigación, la ideación, la creación de lluvias de ideas, mapas mentales y una variedad de otras actividades colaborativas.

### Ventajas
* Facil adaptabilidad para trabajos en grupo
* Interfaz intuitiva

## Git
Git es una herramienta que realiza una función del control de versiones de código de forma distribuida.

### Ventajas
* Es muy potente
* No depende de un repositorio central
* Es software libre
* Con ella podemos mantener un historial completo de versiones
* Podemos movernos, como si tuviéramos un puntero en el tiempo, por todas las revisiones de código y desplazarnos una manera muy ágil.
* Tiene un sistema de trabajo con ramas

## Gitlab
GitLab es una herramienta de ciclo de vida y repositorio de Git. Es un completo DevOps plataforma, que permite a los profesionales gestionar y realizar diversas tareas del proyecto. Las tareas incluyen la planificación del proyecto, la gestión del código fuente, el mantenimiento de la seguridad y el seguimiento.

GitLab fue creado en 2011 por desarrolladores ucranianos llamados Dmitriy Zaporozhets & Valery Sizov. Actualmente tiene más de 100k usuarios y grandes empresas como la NASA, IBM, Sony lo utilizan.

### Ventajas
* Pipelines CI / CD potentes y bien definidos
* Contenedores Docker
* Registro incorporado implementado instantáneamente sin configuración.
* Admite servidores de terceros para administrar imágenes de Docker
* Seguimiento de problemas
* Escrito en Ruby and Go

## GitKraken
GitKraken es una herramienta multi plataforma (Mac, Windows y Linux) que nos ayuda a manejar Git de manera sencilla, lo cual incide en nuestra productividad.

Con GitKraken, Git realmente hace sentido, podemos abrir fácilmente repositorios, organizar favoritos y organizar estos en grupos de proyectos entre otras cosas. GitKraken se integra transparentemente con GitHub, Bitbucket y GitLab y soporta a través de su muy bien lograda interfaz de usuario funcionalidades de arrastrar y soltar para simplificar tareas que pudieren ser complicadas como unir (merge), integrar cambios de una rama a otra (rebase) y empujar o publicar (push).

### Ventajas
* Interfaz intuitiva y facil de usar
* Grafico representativo de las ramas
* Depliegue de informacion

## Hackmd
HackMD es un editor colaborativo de Markdown que permite editar documentos en equipo.

### Ventajas
* Eficiente al momento de trabajar en grupo
* Indicador sobre el aporte de cada integrante
