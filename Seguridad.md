
## Descripcion de la seguridad de la aplicacion :pencil2:

### Amazon Cognito
Amazon Cognito es un servicio administrado que permite agregar rápidamente usuarios para las aplicaciones móviles y web al proporcionar pantallas de inicio de sesión integradas y funcionalidad de autenticación. Maneja la seguridad, la autorización y la sincronización para su proceso de administración de usuarios en todos los dispositivos para todos sus usuarios.

### JWK
Una JSON Web Key ( JWK ) es una estructura JSON que representa una llave criptográfica. Los algoritmos de cifrado están definidos en una especificación diferente. Una llave JWK está compuesta por diferentes propiedades dentro de las cuáles algunas son comunes y otras son específicas para cada tipo de llave.

### MD5
El MD5 (algoritmo de resumen de mensajes) es un protocolo criptográfico que se usa para autenticar mensajes y verificar el contenido y las firmas digitales. El MD5 se basa en una función hash que verifica que un archivo que ha enviado coincide con el que ha recibido la persona a la que se lo ha enviado. Se utilizará MD5 para encriptar las contraseñas de los usuarios.

