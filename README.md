# Tarea1_AYD1



<h1 align="center" >Análisis y diseño 1</h1>

## :computer: Integrantes
|  Nombre | Carnet  |
| ------------- | ------------- |
|Adriana Marié Gómez Dávila|201504236|
|Pablo Daniel Rivas | 201901510   |
|Juan Diego Alvarado Salguero| 201807335 |
|José Ottoniel Sincal Guitzol| 201801375 |
|Eleazar Jared Lopez Osuna| 201700893 |
<br/><br/>



[Metodologia de desarrollo](Metodologia_desarrollo.md)

[Modelo de branching](Modelo_branching.md)

[Toma de requerimientos](Toma_requerimientos.md)

[Mapeo de historias](Mapeo_historias.md)

[Criterios de aceptacion](Criterios_aceptacion.md)

[Tecnologías a utilizar](Tecnologias.md)

[Arquitectura de sistemas](Arquitectura.md)

[Diagrama de almacenamiento](Diagrama_almacenamiento.md)

[Seguridad de la aplicacion](Seguridad.md)

[Mockups](Mockups.md)

[Diagrama de clase](Diagrama_clase.md)

[Diagrama de casos de uso](CDU.md)



