
## Descripción de modelo de branching a utilizar :pencil2:
### Beneficios del modelo
El trabajo con ramas resulta muy cómodo en el desarrollo de proyectos, porque es posible que todas las ramas creadas evolucionen al mismo tiempo, pudiendo el desarrollador pasar de una rama a otra en cualquier momento según las necesidades del proyecto. Si en un momento dado el trabajo con una rama nos ha resultado interesante, útil y se encuentra estable, entonces podemos fusionar ramas para incorporar las modificaciones del proyecto en su rama principal.

### feature/Login
En esta rama, se manejará el inicio de sesión de los usuarios y la creación cuentas nuevas. También se implementará la seguridad de inicio de sesión, esto para brindar una mejor seguridad a los perfiles de los usuarios.

### feature/ServicioHotel
Esta rama se encargará de los servicios completos que ofrecen los hoteles. 
Se implementará la opción de crear nuevas habitaciones para el hotel, asignándoles precios y la zona o lugar donde se encuentra dicha habitación de hotel.
También se agregará una opción de filtros para que los usuarios puedan no solo buscar hoteles, sino también filtrar sus búsquedas de manera intuitiva.

### feature/ServicioAuto
Esta rama se encargará de los servicios completos que ofrece el alquiler de autos. 
Se implementará la opción de crear nuevos autos; asignándoles su marca, modelo, placa y precio.
También se agregará una opción de filtros para que los usuarios puedan no solo buscar autos, sino también filtrar sus búsquedas de manera intuitiva.

### feature/ServicioVuelo
Esta rama se encargará de los servicios completos que ofrecen los vuelos.
Se implementará la opción de crear nuevos vuelos, asignándoles precio, fecha y asientos totales del vuelo.
También se agregará una opción de filtros para que los usuarios puedan no solo buscar vuelos, sino también filtrar sus búsquedas de manera intuitiva.

### feature/Admin
En esta rama se agregarán las funcionalidades del administrador. 
Entre estas funcionalidades se incluye el crear servicios de hotel, renta de autos y servicios de vuelo. También se implementará un dashboard que muestre y filtre los servicios que hay en funcionamiento.

### feature/Turista
En esta rama se agregarán las funcionalidades del turista. 
Se implementará un navbar que clasifique los diferentes servicios para que el usuario pueda acceder a ellos de manera fácil e intuitiva. Dentro de cada clasificación se manejará un dashboard que muestre todos los vuelos, autos u hoteles (según la selección del turista) y también se agregará un filtrado para que el turista pueda visualizar los servicios según sus preferencias.

### feature/ManejoReseñas
Esta rama tendrá como objetivo que el turista pueda dar reseñas de sus experiencias al haber adquirido los servicios registrados en la página. 
Se implementará un apartado en el navbar del turista, donde dirija a una encuesta de reseña para que seleccione el servicio que adquirió y agregue en un campo de texto su experiencia con el servicio.
También se agregará una opción de visualizar sus reseñas obtenidas a todos los servicios; además el administrador podrá visualizar en su dashboard las reseñas que obtuvieron los servicios.
