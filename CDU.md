
## Diagrama de Casos de uso :pencil2:
### CDU manejo de cuenta
![](https://i.imgur.com/z3rycHM.jpg)

### CDU manejo de hoteles
![](https://i.imgur.com/ps0fbBD.jpg)

### CDU manejo de autos
![](https://i.imgur.com/NnyLMei.jpg)

### CDU manejo de vuelos
![](https://i.imgur.com/KWEad7f.jpg)

### CDU manejo de reseñas
![](https://i.imgur.com/n1y545Z.jpg)

